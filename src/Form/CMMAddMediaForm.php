<?php

namespace Drupal\cocoon_media\Form;

use Drupal\cocoon_media\CocoonController;
use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class CMMAddMediaForm.
 *
 * @package Drupal\cocoon_media\Form
 */
class CMMAddMediaForm extends FormBase {

  /**
   * Default settings.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The cocoonController.
   *
   * @var \Drupal\cocoon_media\CocoonController
   */
  protected $cocoonController;

  /**
   * Duration setting of cache.
   *
   * @var float|int
   */
  protected $cacheDuration;

  /**
   * TODO replace with interface constants.
   *
   * @var array
   */
  protected array $fileTypeImage = [
    'jpg',
    'jpeg',
    'png',
    'gif',
    'tiff',
    'tif',
    'bmp',
  ];

  /**
   * TODO replace with interface constants.
   *
   * @var array
   */
  protected array $fileTypeVideo = [
    'mp4',
    'avi',
    'flv',
    'mov',
  ];

  /**
   * The bundle name to save images to.
   *
   * @var array|mixed|string|null
   */
  protected $mediaImageBundle = '';

  /**
   * The bundle name to save videos to.
   *
   * @var array|mixed|string|null
   */
  protected $mediaVideoBundle = '';

  /**
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system, FileUrlGeneratorInterface $url_generator, ConfigFactoryInterface $config_factory) {
    $this->fileSystem = $file_system;
    $this->urlGenerator = $url_generator;
    $this->config = $config_factory->get('cocoon_media.settings');
    $this->cocoonController = new CocoonController(
      $this->config->get('cocoon_media.domain'),
      $this->config->get('cocoon_media.username'),
      $this->config->get('cocoon_media.api_key')
    );
    $this->mediaImageBundle = $this->config->get('cocoon_media.media_image_bundle');
    $this->mediaVideoBundle = $this->config->get('cocoon_media.media_video_bundle');
    $this->cacheDuration = $this->config->get('cocoon_media.cache_duration') ?: 60 * 5;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('file_url_generator'),
      $container->get('config.factory')
     );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cocoon_media_add_media_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['cocoon_media_browser'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cocoon Media Management Browse'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    ];
    // CMM Label.
    $form['cocoon_media_browser']['othertable'] = [
      '#type' => 'tablegridselect',
    ];
    // CMM Label.
    $form['cocoon_media_browser']['description'] = [
      '#markup' => $this->t("Browse and add Cocoon Media to your library.") . '<br/>',
    ];

    // Add the following form elements only if the module API is configured.
    if (!empty($this->config->get('cocoon_media.api_key'))
      && !empty($this->config->get('cocoon_media.domain'))
      && !empty($this->config->get('cocoon_media.username'))) {
      $form['cocoon_media_browser']['clear_cache'] = [
        '#type' => 'button',
        '#value' => $this->t('Refresh library'),
        '#ajax' => [
          'callback' => [$this, 'refreshLibrary'],
          'wrapper' => 'edit-cocoon-media-browser',
          'effect' => 'fade',
          'prevent' => 'onfocus',
          'keypress' => TRUE,
        ],
      ];
      $sets = $this->cocoonController->getSets();
      $radio_sets = [];
      $total_count = 0;
      foreach ($sets as $set) {
        $radio_sets[$set['id']] = $set['title'] . ' (' . $set['file_count'] . ')';
        $total_count += $set['file_count'];
      }
      $radio_sets['all'] = $this->t('All (@count) Only showing the first 30 items if "no search by tag" is performed', ['@count' => $total_count]);
      $form['cocoon_media_browser']['sets'] = [
        '#type' => 'radios',
        '#title' => $this->t('Select a set'),
        '#default_value' => 'all',
        '#options' => $radio_sets,
        '#ajax' => [
          'callback' => [$this, 'ajaxCallbackGetFilesBySet'],
          'wrapper' => 'cocoon-results',
          'effect' => 'fade',
        ],
      ];

      $set = 'all';
      $tag_name = '';
      $current_page = 0;

      $values = $form_state->getValues();
      if (!empty($values)) {
        $set = $values['cocoon_media_browser']['sets'];
        $tag_name = $values['cocoon_media_browser']['tag_elements']['tagname'];
        $current_page = $values['cocoon_media_browser']['results']['pager_actions']['page'];
        if (isset($values['op']) && !$values['op'] instanceof TranslatableMarkup) {
          if ($values['op'] == '>') {
            $current_page += 1;
          }
          if ($values['op'] == '<') {
            $current_page -= 1;
          }
        }
      }

      $paging_size = $this->config->get('cocoon_media.paging_size') ?? 15;
      $options = $this->buildOptionsElements($set, $tag_name);
      $options_chunk = array_chunk($options, $paging_size, TRUE);
      $total_pages = count($options_chunk) ?? 0;
      $current_page = max($current_page, 0);
      $current_page = $current_page >= $total_pages ? $total_pages - 1 : $current_page;

      $form['cocoon_media_browser']['tag_elements'] = [
        '#prefix' => '<div class="container-inline">',
        '#suffix' => '</div>',
      ];
      $form['cocoon_media_browser']['tag_elements']['tagname'] = [
        '#type' => 'textfield',
        '#placeholder' => $this->t('Search by tag'),
        '#autocomplete_route_name' => 'cocoon_media.tag_autocomplete',
        '#size' => '20',
        '#maxlength' => '60',
      ];
      $form['cocoon_media_browser']['tag_elements']['tag_search'] = [
        '#type' => 'button',
        '#value' => $this->t('Search'),
        '#ajax' => [
          'callback' => [$this, 'ajaxCallbackGetFilesBySet'],
          'wrapper' => 'cocoon-results',
          'effect' => 'fade',
          'prevent' => 'onfocus',
          'keypress' => TRUE,
        ],
      ];

      $form['cocoon_media_browser']['results'] = [
        '#prefix' => '<div id="cocoon-results">',
        '#suffix' => '</div>',
      ];

      $ajax_call = [
        'callback' => [$this, 'ajaxCallbackGetFilesBySet'],
        'wrapper' => 'cocoon-results',
        'effect' => 'fade',
        'progress' => [
          'message' => '',
        ],
      ];

      $form['cocoon_media_browser']['results'] = array_merge($form['cocoon_media_browser']['results'], $this->buildAjaxPager($ajax_call, $current_page, $total_pages));
      $table_select_options = [];
      if (isset($options_chunk[$current_page])) {
        $table_select_options = $options_chunk[$current_page];
      }
      $form['cocoon_media_browser']['results']['images_table'] = $this->buildTableSelect('images-table', $table_select_options);
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Download Media'),
        '#button_type' => 'primary',
        '#states' => [
          'visible' => [
            ':input[name="cocoon_media_browser_api_settings_link"]' => ['empty' => FALSE],
          ],
        ],
      ];
    }
    else {
      // CMM Label.
      $url = Link::createFromRoute('here', 'cocoon_media.admin_settings');
      $form['cocoon_media_browser']['api_not_configured'] = [
        '#markup' => $this->t("Please first add the configuration parameters here:"),
      ];
      $form['cocoon_media_browser']['api_settings_link'] = $url->toRenderable();
      unset($form['actions']['submit']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $selected_images = $values['cocoon_media_browser']['results']['images_table'];
    $filenames = '';
    foreach ($selected_images as $selected_image_id) {
      if ($selected_image_id) {
        $file_info = $this->cocoonController->getThumbInfo($selected_image_id);
        if (!empty($file_info['faultstring'])) {
          $this->messenger()
            ->addMessage($this->t("The File(s) cannot be added to the media library. Error message: %error", [
              '%error' => $file_info['faultstring'],
            ]), MessengerInterface::TYPE_ERROR);
          return;
        }
        $url = $file_info['path'];
        // Check the cache and download the file if needed.
        /** @var \Drupal\file\Entity\File $file */
        $file = $this->retrieveRemoteFile($url);
        if (empty($file)) {
          $this->messenger()
            ->addMessage($this->t("The File(s) cannot be added to the media library."), 'error');
          return;
        }

        $media_bundle = 'file';
        $field_media_name = 'field_media_file';

        if (in_array($file_info['ext'], $this->fileTypeImage, TRUE)) {
          // TODO replace with generic image bundle
          // or make configurable what the bundle is.
          $media_bundle = 'image';
          if ($this->mediaImageBundle) {
            $media_bundle = $this->mediaImageBundle;
          }
          $field_media_name = 'field_media_image';
        }

        if (in_array($file_info['ext'], $this->fileTypeVideo, TRUE)) {
          // TODO replace with generic video bundle
          // or make configurable what the bundle is.
          $media_bundle = 'video';
          if ($this->mediaImageBundle) {
            $media_bundle = $this->mediaVideoBundle;
          }
          $field_media_name = 'field_media_video_file';
        }

        // Create media entity with saved file.
        // TODO use entityStorageManager.
        $media = Media::create([
          'bundle' => $media_bundle,
          'langcode' => \Drupal::languageManager()
            ->getDefaultLanguage()
            ->getId(),
          'name' => $file_info['name'],
          $field_media_name => [
            'target_id' => $file->id(),
            'alt' => $file_info['name'],
            'title' => $file_info['name'],
          ],
        ]);

        // TODO check media bundle exist before calling media->save();
        // if not show error below:
        // drupal_set_message($this->t("Have you set the correct bundle
        // in the cocoon_media module"), 'error');.
        $media->save();

        // TODO use dependency injection.
        $media->setOwnerId(\Drupal::currentUser()->id());
        $filenames .= $file_info['name'] . ', ';
      }
    }
    // Redirecting to the media library page.
    $media_url = Url::fromRoute('entity.media.collection');
    $form_state->setRedirectUrl($media_url);
    $filenames = substr($filenames, 0, -2);
    // Adding custom message.
    $this->messenger()
      ->addMessage($this->t('The File(s) <i>%filenames</i> has been added to the media library.', [
        '%filenames' => $filenames,
      ]));
  }

  /**
   * TODO add function description.
   *
   * @param string $url
   *   TODO add url description.
   * @param string $local_url
   *   TODO add description.
   * @param bool $to_temp
   *   TODO add description.
   *
   * @return string
   *   TODO add description.
   */
  private function retrieveRemoteFile($url, $local_url = '', $to_temp = FALSE) {
    // Check the cache and download the file if needed.
    $parsed_url = parse_url($url);
    $cocoon_dir = 'cocoon_media_files';
    $cocoon_media_directory = 'public://' . $cocoon_dir . '/';
    $this->fileSystem->prepareDirectory($cocoon_media_directory, $this->fileSystem::CREATE_DIRECTORY);
    if (empty($local_url)) {
      // $cocoon_media_directory = $to_temp ? 'temporary://' : 'public://';.
      $local_url = $cocoon_media_directory . $this->fileSystem->basename($parsed_url['path']);
    }
    return system_retrieve_file($url, $local_url, !$to_temp, $this->fileSystem::EXISTS_REPLACE);
  }

  /**
   * TODO add function description.
   *
   * @param array $image_info
   *   Image info containing filename and extension key/values.
   * @param string $prefix
   *   Prefix before image name.
   *
   * @return string
   *   Public path.
   */
  private function remoteThumbToLocal(array $image_info, $prefix) {
    $filename = $prefix . $image_info['filename'] . '.' . $image_info['extension'];
    $public_path = 'public://cocoon_media_files/' . $filename;
    $local_path = $this->fileSystem->realpath($public_path);
    if (!in_array($image_info['extension'], $this->fileTypeImage, TRUE)) {
      return '';
    }
    if (!file_exists($local_path)) {
      $thumb_info = $this->cocoonController->getThumbInfo($image_info['id']);
      if (empty($thumb_info['web'])) {
        return '';
      }
      $this->retrieveRemoteFile($thumb_info['web'], $public_path);
    }
    return $public_path;
  }

  /**
   * Search for images with given tag name.
   *
   * @param string $tag_name
   *   The name of the tag.
   *
   * @return array
   *   list of images.
   */
  public function getFilesByTag(string $tag_name): array {
    $tags_images_list = [];
    $matches = [];
    $tags_list = $this->cocoonController->getCachedData('cocoon_media:all_tags', [
      $this->cocoonController,
      'getTags',
    ], [], $this->cacheDuration);

    // Do not search for tag name if the tag name is empty.
    if (!$tag_name) {
      $matches = array_column($tags_list, 'id');
    }

    if ($tag_name) {
      foreach ($tags_list as $tag) {
        $string_found = strpos($tag['name'], $tag_name);
        if ($string_found !== FALSE) {
          $matches[] = $tag['id'];
        }
      }
    }

    foreach ($matches as $tag_id) {
      $tag_files = $this->cocoonController->getCachedData('cocoon_media:tag_' . $tag_id, [
        $this->cocoonController,
        'getFilesByTag',
      ], [$tag_id], $this->cacheDuration);
      $tags_images_list = array_merge($tags_images_list, $tag_files);
    }
    return $tags_images_list;
  }

  /**
   * Ajax callback.
   *
   * @param array $ajax_callback
   *   Cocoon media browser results.
   * @param int $current_page
   *   The current page.
   * @param int $total_pages
   *   Total number of pages.
   *
   * @return array
   *   Renderable array.
   */
  public function buildAjaxPager(array $ajax_callback, $current_page = 0, $total_pages = 0) {
    $form_ajax_pager['pager_actions'] = [
      '#type' => 'actions',
      '#weight' => 0,
    ];
    $form_ajax_pager['pager_actions']['prev'] = [
      '#type' => 'button',
      '#value' => '<',
      '#ajax' => $ajax_callback,
    ];
    $form_ajax_pager['pager_actions']['page'] = [
      '#type' => 'hidden',
      '#value' => $current_page,
    ];
    $form_ajax_pager['pager_actions']['pagenum'] = [
      '#type' => 'button',
      '#value' => $this->t('@current_page of @total_pages', ['@current_page' => $current_page +1, '@total_pages' => $total_pages]),
      '#disabled' => TRUE,
    ];
    $form_ajax_pager['pager_actions']['next'] = [
      '#type' => 'button',
      '#value' => '>',
      '#ajax' => $ajax_callback,
    ];

    return $form_ajax_pager;
  }

  /**
   * Build single option element.
   *
   * @param array $image_info
   *   The array to render the media item from.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The rendered HTML.
   */
  public function buildSingleOptionElement(array $image_info) {

    $thumb_url = '/' . \Drupal::service('extension.list.module')->getPath('cocoon_media')
      . '/images/generic.png';
    $image_size = 100;
    $thumb = $this->remoteThumbToLocal($image_info, 'thumb_');
    if (!empty($thumb)) {
      $thumb_url = $this->urlGenerator->generateString($thumb);
      $image_size = getimagesize(urldecode($this->fileSystem->realpath($thumb)))[1] ?? 100;
    }

    $elm = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
    ];
    $elm['id'] = [
      '#type' => 'hidden',
      '#value' => $image_info['id'],
    ];
    $elm['thumb'] = [
      '#type' => 'label',
      '#title_display' => 'before',
      '#title' => '&nbsp;',
      '#attributes' => [
        'class' => 'media-thumb',
        'style' => "background-image:url(" . $thumb_url . "); background-repeat: no-repeat;background-size: contain;width:100%;height:" . $image_size . "px;max-height:150px",
      ],
    ];
    $elm['title'] = [
      '#type' => 'label',
      '#title_display' => 'before',
      '#title' => $image_info['title'],
      '#attributes' => ['class' => 'media-title'],
    ];
    $elm['file_details'] = [
      '#markup' => '<p><b>Extension: </b>'
      . $image_info['extension']
      . '<br/><b>Size: </b>'
      . round($image_info['size'] / 1024, 2)
      . 'KB</p>',
    ];
    return \Drupal::service('renderer')->renderPlain($elm);
  }

  /**
   * Build an option list with media items to select.
   *
   * @param string $set_id
   *   The id of the set.
   * @param string $tag_name
   *   The name of the tag to filter the files on.
   *
   * @return array
   *   Options with rendered items.
   */
  public function buildOptionsElements($set_id, $tag_name) {
    $save_tag_name = Html::escape($tag_name);
    $cid = 'cocoon_media:options_file_list:' . $set_id . ':' . $save_tag_name;
    $cache = \Drupal::cache()->get($cid);
    if ($cache) {
      return $cache->data;
    }

    $file_list = $this->getImagesBySetId($set_id);
    if ($tag_name) {
      $file_list = $this->reduceFileListByTag($file_list, $tag_name);
    }
    $options = [];
    $i = 0;
    foreach ($file_list as $image_info) {
      $i++;
      $rendered_item = $this->cocoonController->getCachedData('cocoon_media:option_item_' . $image_info['id'], [
        $this,
        'buildSingleOptionElement',
      ], [$image_info]);
      $options[$image_info['id']] = [
        'media_item' => $rendered_item,
      ];
      // TODO Make this nicer, now it only loads the first 30 items.
      if ($set_id === 'all' && $tag_name === '' && $i >= 30) {
        break;
      }
    }
    $expire_time = CacheBackendInterface::CACHE_PERMANENT;
    \Drupal::cache()->set($cid, $options, $expire_time, ['cocoon_media']);
    return $options;
  }

  /**
   * Get Images by set id.
   *
   * @param mixed $set_id
   *   String 'all' or Int with set_id.
   *
   * @return array
   *   Images.
   */
  private function getImagesBySetId($set_id = 'all') {
    $images = [];

    if ($set_id !== 'all') {
      $results = $this->cocoonController->getCachedData('cocoon_media:set_' . $set_id, [
        $this->cocoonController,
        'getFilesBySet',
      ], [$set_id], $this->cacheDuration);
      if ($results) {
        $images = $results;
      }
    }

    if ($set_id == 'all') {
      $sets = $this->cocoonController->getSets();
      foreach ($sets as $set) {
        $images = array_merge($images, $this->cocoonController->getCachedData('cocoon_media:set_' . $set['id'], [
          $this->cocoonController,
          'getFilesBySet',
        ], [$set['id']], $this->cacheDuration));
      }
    }

    return $images;
  }

  /**
   * Build table select list.
   *
   * @param string $hmtlAttributeId
   *   The html id given for this tableselect list.
   * @param array $options
   *   Table select options.
   *
   * @return array
   *   Renderable array.
   */
  public function buildTableSelect($hmtlAttributeId, array $options) {
    $header = [
      'media_item' => $this->t('Media File'),
    ];
    $table = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('No media found'),
      '#multiple' => TRUE,
      '#attributes' => ['id' => $hmtlAttributeId],
      '#cache' => [
        // Cached for one day.
        'max-age' => 60 * 60 * 24,
      ],
      '#attached' => [
        'library' => ['cocoon_media/tablegrid-select'],
      ],
    ];
    return $table;
  }

  /**
   * Get cocoon media browser results.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   The cocooon media browser form results.
   */
  public function ajaxCallbackGetFilesBySet(array &$form, FormStateInterface &$form_state) {
    return $form['cocoon_media_browser']['results'];
  }

  /**
   * Clear cache.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   The cocoon media browser form.
   */
  public function refreshLibrary(array &$form, FormStateInterface &$form_state) {
    \Drupal::cache()->invalidateTags(['cocoon_media']);
    return $form['cocoon_media_browser'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cocoon_media.settings',
    ];
  }

  /**
   * Only keep the files that have both the set and the tag.
   */
  private function reduceFileListByTag(array $file_list, $tag_name) {
    if (empty($file_list)) {
      return [];
    }
    $files_by_tag = $this->getFilesByTag($tag_name);

    if (empty($files_by_tag)) {
      return [];
    }

    $result = [];
    foreach ($files_by_tag as $file_by_tag) {
      if (in_array($file_by_tag['id'], array_column($file_list, 'id'))) {
        $result[] = $file_by_tag;
      }
    }
    return $result;
  }

}

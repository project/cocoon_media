<?php

namespace Drupal\cocoon_media;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use SoapClient;
use SoapFault;
use SoapHeader;
use StdClass;

/**
 * Cocoon controller.
 */
class CocoonController {

  /**
   * The cocoon domain name.
   *
   * @var string
   */
  public static $domainName = 'use-cocoon.nl';

  /**
   * The number of thumbs per page.
   *
   * @var int
   */
  public $thumbsPerPage = 24;

  /**
   * The sub domain configuration.
   *
   * @var string
   */
  public $subdomain = '';

  /**
   * The user name.
   *
   * @var string
   */
  public $username = '';

  /**
   * API key.
   *
   * @var string
   */
  public $secretkey = '';

  /**
   * CocoonController constructor.
   *
   * @param $sub_domain
   * @param $user_name
   * @param $secret_key
   */
  public function __construct($sub_domain, $user_name, $secret_key) {
    $this->subdomain = $sub_domain;
    $this->username = $user_name;
    // $requestId = $reqId;
    $this->secretkey = $secret_key;
  }

  /**
   * Get the tags from the API.
   *
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   *   TODO check result and add to this documentation.
   *
   * @throws \SoapFault
   */
  public function getTags() {
    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getTags();
    }
    catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
    }

    return $output;
  }

  /**
   * The SoapClient.
   *
   * @param string $reqId
   *   TODO document this variable.
   * @param string $sub_domain
   *   The subdomain.
   * @param string $user_name
   *   The user name.
   * @param string $secret_key
   *   The secret key
   *
   * @return \soapClient
   *   The soap client.
   *
   * @throws \SoapFault
   */
  public static function soapClient($reqId, $sub_domain, $user_name, $secret_key) {
    $domainName = self::$domainName;
    $requestId = $reqId;
    $wsdl = "https://{$sub_domain}.{$domainName}/webservice/wsdl";

    $hash = sha1($sub_domain . $user_name . $requestId . $secret_key);

    $oAuth = new StdClass();
    $oAuth->username = $user_name;
    $oAuth->requestId = $requestId;
    $oAuth->hash = $hash;

    $oSoapClient = new SoapClient($wsdl, ['exceptions' => 0]);
    $soap_header = new SoapHeader('auth', 'authenticate', $oAuth);
    $oSoapClient->__setSoapHeaders($soap_header);

    return $oSoapClient;
  }

  /**
   * The microtimestamp as string value.
   *
   * @return string
   */
  public function getRequestId() {
    return (string) microtime(TRUE);
  }

  /**
   * TODO Check if this is used.
   * @param $tagId
   *
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getTag($tagId) {
    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getTag($tagId);
    } catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
    }

    return $output;
  }

  /**
   * @param $tagId
   *
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getFilesByTag($tagId) {
    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getFilesByTag($tagId);
    } catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
    }

    return $output;
  }

  /**
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getSets() {
    $cid = 'cocoon_media:get_sets';
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getSets();
    }
    catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
      \Drupal::messenger()->addError('Please make sure soap is installed, and the configuration is filled in');
      \Drupal::logger('php')->error($oSoapFault->getMessage());
    }
    $expire_time = CacheBackendInterface::CACHE_PERMANENT;
    \Drupal::cache()->set($cid, $output, $expire_time, ['cocoon_media']);
    return $output;
  }

  /**
   * @param $setId
   *
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getFilesBySet($setId) {
    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getFilesBySet($setId);
    }
    catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
    }

    return $output;
  }

  /**
   * @param $fileId
   *
   * @return array
   */
  public function getThumbInfo($fileId) {
    try {
      $subDomain = $this->subdomain;
      $domainName = self::$domainName;
      $url = "https://{$subDomain}.{$domainName}";
      $thumbOrg = 'original';

      $thumbWeb = '500px';

      $noThumb = TRUE;

      $aThumbTypes = $this->getThumbTypes();
      if (array_keys($aThumbTypes)[0]) {
        $thumbWeb = array_keys($aThumbTypes)[0];
      }
      $thumbOrgPath = $aThumbTypes[$thumbOrg]['path'];
      $thumbWebPath = $aThumbTypes[$thumbWeb]['path'];

      $aFile = $this->getFile($fileId);
      if (is_array($aFile)) {
        $filename = $aFile['filename'];
        $extention = strtolower($aFile['extension']);

        if ($extention === 'jpg' ||
          $extention === 'jpeg' ||
          $extention === 'png' ||
          $extention === 'gif' ||
          $extention === 'tiff' ||
          $extention === 'tif' ||
          $extention === 'bmp'
        ) {
          $noThumb = FALSE;
        }

        $fileDim = $aFile['width'] && $aFile['height'] ? $aFile['width'] . ' x ' . $aFile['height'] : '';
        $fileSize = $aFile['size'] ? round($aFile['size'] / 1024) . ' KB' : '';

        if ($aFile['upload_date']) {
          $date = date_create($aFile['upload_date']);
          $fileUploaded = $date;
        }
        else {
          $fileUploaded = '';
        }

        $thumb_ext = 'jpg';
        if ($extention == 'gif' || $extention == 'png' || $extention == 'jpg') {
          $thumb_ext = $extention;
        }
        return [
          'path' => $url . $thumbOrgPath . '/' . $filename . '.' . $extention,
          'web' => !$noThumb ? $url . $thumbWebPath . '/' . $filename . '.' . $thumb_ext : '',
          'ext' => $extention,
          'name' => $filename,
          'dim' => $fileDim,
          'size' => $fileSize,
          'uploaded' => $fileUploaded,
          'domain' => $url,
        ];
      }
    } catch (SoapFault $oSoapFault) {
      $aFile = $oSoapFault;
    }
    return get_object_vars($aFile);
  }

  /**
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getThumbTypes() {
    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getThumbtypes();
    }
    catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
    }

    return $output;
  }

  /**
   * @param $fileId
   *
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getFile($fileId) {
    try {
      $output = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey)->getFile($fileId);
    }
    catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
      watchdog_exception('file', $oSoapFault);
    }

    return $output;
  }

  /**
   * @return \Drupal\cocoon_media\SoapFault|\Exception
   */
  public function getVersion() {
    try {
      $client = self::soapClient(
        $this->getRequestId(),
        $this->subdomain,
        $this->username,
        $this->secretkey);
      $output = $client->getVersion();
    }
    catch (SoapFault $oSoapFault) {
      $output = $oSoapFault;
    }

    return $output;
  }

  /**
   * Get cached data.
   *
   * @param int $cid
   *   Cache id.
   * @param array $function_name
   *   Containing the object and the method.
   * @param array $params
   *   Parameters for the callback method.
   * @param int $expire_time
   *   Cache expire time.
   *
   * @return mixed|null
   *   The Cached data.
   */
  function getCachedData($cid, array $function_name, array $params = [], int $expire_time = Cache::PERMANENT) {
    if ($cache = \Drupal::cache()->get($cid)) {
      $data = $cache->data;
    }
    else {
      $data = call_user_func_array($function_name, $params);
      $expire_time = $expire_time === -1 ? CacheBackendInterface::CACHE_PERMANENT : time() + $expire_time;
      \Drupal::cache()->set($cid, $data, $expire_time, ['cocoon_media']);
    }
    return $data;
  }

  /**
   * @param $errMsg
   *
   * @return false|string
   */
  private function errorResponse($errMsg) {
    return json_encode(['status' => 'error', 'statusMsg' => $errMsg]);
  }

}

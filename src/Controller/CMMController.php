<?php

namespace Drupal\cocoon_media\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\cocoon_media\CocoonController;

/**
 * Class CMMController.
 *
 * @package Drupal\cocoon_media\Controller
 */
class CMMController extends ControllerBase {

  /**
   * Cocoon_media configuration settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * CocoonController.
   *
   * @var \Drupal\cocoon_media\CocoonController
   */
  protected $cocoonController;

  /**
   * The constructor.
   */
  public function __construct() {
    $this->settings = $this->config('cocoon_media.settings');
    $this->cocoonController = new CocoonController(
      $this->settings->get('cocoon_media.domain'),
      $this->settings->get('cocoon_media.username'),
      $this->settings->get('cocoon_media.api_key')
    );
  }

  /**
   * Get tags using the cocoonController.
   *
   * @param \Symfony\Component\HttpFoundation\Request $req
   *   Request value.
   * @param string $tag_name
   *   The name of the tag.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response from cocoon in json.
   */
  public function getTagsAutocomplete(Request $req, string $tag_name = '') {
    $params = $req->query->get('q');
    $tags_list = $this->cocoonController->getCachedData('cocoon_media:all_tags', [$this->cocoonController, 'getTags']);
    $tagNames = [];
    // Using autocomplete in forms does not work properly with paths,
    // I am adding this 'trick':
    // If tag_name is empty but parameter is not then us parameter.
    $tag_name = $tag_name ?: $params;
    $filterd_tag_list = array_filter($tags_list, static function ($item) use ($tag_name) {
      return strpos($item['name'], $tag_name) === 0;
    });

    foreach ($filterd_tag_list as $filtered_tag) {
      $tagNames[] = [
        'value' => $filtered_tag['name'],
        'label' => $filtered_tag['name'],
      ];
    }

    return new JsonResponse($tagNames);
  }

}
